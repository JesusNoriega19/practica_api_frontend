import { Component, OnInit } from '@angular/core';
import { ApiService } from './../../services/api.service';

@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.css']
})
export class PersonalComponent implements OnInit {

  rellenar:any[]=[];

  constructor(private _serviceCrud: ApiService) {  }

  ngOnInit(): void {
   this.getPersonal();
  }

  getPersonal() {
    this._serviceCrud.getPersonal().subscribe((data: any) => {
      this.rellenar = data.respuesta;
      console.log(data.respuesta);
    });
  }

}
