import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from './../../services/api.service';

@Component({
  selector: 'app-sucursal',
  templateUrl: './sucursal.component.html',
  styleUrls: ['./sucursal.component.css']
})
export class SucursalComponent {

  rellenar:any[]=[];

  constructor(private _serviceCrud: ApiService) {  }

  ngOnInit(): void {
   this.getSucursales();
  }

  getSucursales() {
    this._serviceCrud.getSucursales().subscribe((data: any) => {
      this.rellenar = data.respuesta;
      console.log(data.respuesta);
    });
  }

}
