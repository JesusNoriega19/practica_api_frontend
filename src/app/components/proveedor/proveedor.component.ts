import { Component, OnInit } from '@angular/core';
import { ApiService } from './../../services/api.service';

@Component({
  selector: 'app-proveedor',
  templateUrl: './proveedor.component.html',
  styleUrls: ['./proveedor.component.css']
})
export class ProveedorComponent implements OnInit{

  rellenar:any[]=[];

  constructor(private _serviceCrud: ApiService) {  }

  ngOnInit(): void {
   this.getProveedores();
  }

  getProveedores() {
    this._serviceCrud.getProveedores().subscribe((data: any) => {
      this.rellenar = data.respuesta;
      console.log(data.respuesta);
    });
  }

}
