import { ApiService } from './../../services/api.service';
import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})

export class ProductosComponent implements OnInit {

  rellenar:any[]=[];

  constructor(private _serviceCrud: ApiService) {  }

  ngOnInit(): void {
   this.getProducto();
  }

  getProducto() {
    this._serviceCrud.getProducto().subscribe((data: any) => {
      this.rellenar = data.respuesta;
      console.log(data.respuesta);
    });
  }
}
