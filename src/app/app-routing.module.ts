import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductosComponent } from './components/productos/productos.component';
import { PersonalComponent } from './components/personal/personal.component';
import { SucursalComponent } from './components/sucursal/sucursal.component';
import { ProveedorComponent } from './components/proveedor/proveedor.component';


//RUTAS DE NAVEGACION
const routes: Routes = [
  {path:'',redirectTo: '/productos', pathMatch: 'full'},
  {path:'productos',component:ProductosComponent},
  {path:'personal',component:PersonalComponent},
  {path:'proveedor',component:ProveedorComponent},
  {path:'sucursal',component:SucursalComponent},
 
  //{path:'**',component:PagenotfoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
