import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductosComponent } from './components/productos/productos.component';
import { PersonalComponent } from './components/personal/personal.component';
import { SucursalComponent } from './components/sucursal/sucursal.component';
import { ProveedorComponent } from './components/proveedor/proveedor.component';
import { NavmenuComponent } from './components/navmenu/navmenu.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    ProductosComponent,
    PersonalComponent,
    SucursalComponent,
    ProveedorComponent,
    NavmenuComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
