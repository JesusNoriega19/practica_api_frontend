import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  public URL = "http://localhost:3900";

  constructor(private _http : HttpClient)  {}

  getProducto(){
    const url = `${this.URL}/obtener/datos/producto`;
    console.log(url);
  return this._http.get(url);
  }

  getPersonal(){
    const url = `${this.URL}/obtener/datos/personal`;
    console.log(url);
  return this._http.get(url);
  }

  getProveedores(){
    const url = `${this.URL}/obtener/datos/proveedor`;
    console.log(url);
  return this._http.get(url);
  }

  getSucursales(){
    const url = `${this.URL}/obtener/datos/sucursal`;
    console.log(url);
  return this._http.get(url);
  }

}
